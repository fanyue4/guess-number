package answer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Answer {
    
    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;
    
    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }
    
    public List<Integer> getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }
    
    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }
    
    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        // Need to be implemented
        List<Integer> answerList = new ArrayList<>();
        try {
            Path path = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(filePath.toString())).toURI());
            Stream<String> lines = Files.lines(path);
            String stringAnswer = lines.collect(Collectors.joining());
            lines.close();

            String[] splitStringAnswer = stringAnswer.split("");
            for (String s : splitStringAnswer) {
                answerList.add(Integer.parseInt(s));
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return answerList;
    }
    
    private List<Integer> generateRandomAnswer() {
        // Need to be implemented
        Random random = new Random();
        Set<Integer> randomSet = new HashSet<>();
        while (randomSet.size() < ANSWER_LENGTH) {
            randomSet.add(random.nextInt(ANSWER_NUMBER_LIMIT));
        }
        return new ArrayList<>(randomSet);
    }
    
    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        // Need to be implemented
        if (answer.stream().distinct().count() != ANSWER_LENGTH) {
            throw new InvalidAnswerException("Wrong input.");
        }
    }
}
