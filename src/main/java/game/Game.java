package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Game {
    
    private Answer answer;
    private Map<String, String> guessResult;
    
    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }
    
    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }
    
    public boolean isOver() {
        // Need to be implemented
        GuessResult guessResult = new GuessResult(this.guessResult);
        return guessResult.getGameResult() == GameResult.WIN || guessResult.getGameResult() == GameResult.LOST;
    }
    
    public String getResult() {
        // Need to be implemented
        String result = "";
        GuessResult guessResult = new GuessResult(this.guessResult);
        if (guessResult.getGameResult() == GameResult.WIN) {
            result += guessResult.getResult() + "\nCongratulations, you win!";
        } else {
            result += guessResult.getResult() + "\nUnfortunately, you have no chance, the answer is 1234!";
        }
        return result;
    }
    
    private int getBSize(List<Integer> numbers) {
        // Need to be implemented
        int count = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (!numbers.get(i).equals(answer.getAnswer().get(i)) && answer.getAnswer().contains(numbers.get(i))) {
                count += 1;
            }
        }
        return count;
    }
    
    private int getASize(List<Integer> numbers) {
        // Need to be implemented
        int count = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i).equals(answer.getAnswer().get(i))) {
                count += 1;
            }
        }
        return count;
    }
}
