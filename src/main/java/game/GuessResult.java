package game;

import java.util.Map;
import java.util.Optional;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;
    
    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }
    

    public String getResult() {
        // Need to be implemented
        return guessRecord.entrySet().stream()
                .map(entry -> String.join(" ", entry.getKey(),entry.getValue()))
                .reduce((a, b) -> a + "\n" + b).get();
    }
    
    public GameResult getGameResult() {
        // Need to be implemented
        GameResult result = null;
        if (guessRecord.size() < CHANCE_LIMIT && !guessRecord.containsValue("4A0B")) {
            result = GameResult.NORMAL;
        } else if (guessRecord.size() <= CHANCE_LIMIT && guessRecord.containsValue("4A0B")) {
            result = GameResult.WIN;
        } else if (guessRecord.size() >= CHANCE_LIMIT && !guessRecord.containsValue("4A0B")) {
            result =  GameResult.LOST;
        }
        return result;
    }
}
